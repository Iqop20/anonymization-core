package inconnu.anonymization.core.databases.dynamic.utils;

import org.apache.lucene.util.SloppyMath;

import java.math.BigDecimal;

/**
 * Class to hold location values
 */
public class GeoLocationPoint {
    public double latitude;
    public double longitude;
    public double altitude=Double.MIN_VALUE;

    public GeoLocationPoint(double latitude, double longitude, double altitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }

    public GeoLocationPoint(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public String toString() {
        if (altitude==Double.MIN_VALUE) {
            return "(" + new BigDecimal(this.latitude).toPlainString() + "," + new BigDecimal(this.longitude).toPlainString()+ ")";
        }
        return "(" + new BigDecimal(this.latitude).toPlainString() + "," + new BigDecimal(this.longitude).toPlainString()+ ","+new BigDecimal(this.altitude).toPlainString()+")";
    }


    public double distanceTo(GeoLocationPoint p){
        return SloppyMath.haversinMeters(this.latitude,this.longitude,p.latitude,p.longitude);
    }
}
