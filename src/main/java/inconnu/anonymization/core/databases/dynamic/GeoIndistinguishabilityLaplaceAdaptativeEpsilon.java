package inconnu.anonymization.core.databases.dynamic;

import inconnu.anonymization.core.databases.dynamic.utils.GeoLocationPoint;
import inconnu.anonymization.core.databases.dynamic.utils.PlanarLaplace;
import org.apache.commons.math3.stat.regression.SimpleRegression;

/**
 * This is the implmentation of the Adaptative Geo-Indistinguishability algorithm that uses linear regression to check the if the attacker is capable of predicting the next location and apply harder anonymization levels on those cases
 * This is based on the pseudo-code available on the paper (https://link.springer.com/article/10.1007/s11276-017-1534-x#Sec16)
 */
public class GeoIndistinguishabilityLaplaceAdaptativeEpsilon {
    double alpha;
    double epsilon;
    double delta1;
    double beta;
    double delta2;
    SimpleRegression regression;

    /**
     * If the distance between the prediction and the real location is less than delta1 the geo-indistinguishability epsilon is multiplied by alpha.<br>
     * If the distance between the prediction and the real location bigger than delta2 the geo-indistinguishability epsilon is multiplied by beta.<br>
     * Otherwise the geo-indistinguishability epsilon is not altered
     * epsilon = radius*l
     * @param radius The anonymization radius
     * @param alpha The alpha value to be multiplied to epsilon when the distance between the real and sanitized is lower than delta1
     * @param delta1 The highest distance between real and sanitized location where epsilon = epsilon * alpha
     * @param beta The beta value to be multiplied to epsilon when the distance between the real and sanitized is higher than delta2
     * @param delta2 The lowest distance between real and sanitized location from which epsilon = epsilon * beta
     */
    public GeoIndistinguishabilityLaplaceAdaptativeEpsilon(double radius,double alpha, double delta1, double beta, double delta2) {
        this.alpha = alpha;
        this.epsilon = Constants.l/radius;
        this.delta1 = delta1;
        this.beta = beta;
        this.delta2 = delta2;
        this.regression = new SimpleRegression();
    }

    /**
     * If the distance between the prediction and the real location is less than delta1 the geo-indistinguishability epsilon is multiplied by alpha.<br>
     * If the distance between the prediction and the real location bigger than delta2 the geo-indistinguishability epsilon is multiplied by beta.<br>
     * Otherwise the geo-indistinguishability epsilon is not altered
     * epsilon = radius*l
     * @param radius The anonymization radius
     * @param l The distinguishability level
     * @param alpha The alpha value to be multiplied to epsilon when the distance between the real and sanitized is lower than delta1
     * @param delta1 The highest distance between real and sanitized location where epsilon = epsilon * alpha
     * @param beta The beta value to be multiplied to epsilon when the distance between the real and sanitized is higher than delta2
     * @param delta2 The lowest distance between real and sanitized location from which epsilon = epsilon * beta
     */
    public GeoIndistinguishabilityLaplaceAdaptativeEpsilon(double radius,double l,double alpha, double delta1, double beta, double delta2) {
        this.alpha = alpha;
        this.epsilon = l/radius;
        this.delta1 = delta1;
        this.beta = beta;
        this.delta2 = delta2;
        this.regression = new SimpleRegression();
    }

    /** Generate the sanitized point by first doing a linear regression using the previous real location points and verifying if the linear regression guess for the point longitude given the real point latitude is close to the real location.<br>
     * If the distance between the prediction and the real location is less than delta1 the geo-indistinguishability epsilon is multiplied by alpha.<br>
     * If the distance between the prediction and the real location bigger than delta2 the geo-indistinguishability epsilon is multipled by beta.<br>
     * Otherwise the geo-indistinguishability epsilon is not altered
     * @param point real location point
     * @return sanitized point
     */
    public GeoLocationPoint sanitizeNewPoint(GeoLocationPoint point){
        double predict=regression.predict(point.latitude);
        double currentEpsilon=epsilon;

        if (!Double.toString(predict).equals("NaN")){

            double distance = point.distanceTo(new GeoLocationPoint(point.latitude, predict));
            if (distance< delta1){
                currentEpsilon = epsilon*alpha;
            }else if (distance >= delta2){
                currentEpsilon = epsilon*beta;
            }
        }

        regression.addData(point.latitude,point.longitude);

        PlanarLaplace planarLaplace = new PlanarLaplace();

        return planarLaplace.addNoise(currentEpsilon,point);
    }


}
