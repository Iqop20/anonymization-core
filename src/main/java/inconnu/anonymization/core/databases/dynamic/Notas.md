#Notes from location-guard reverse-engineering

Accuracy = Math.round((new PlanarLaplace).alphaDeltaAccuracy(epsilon/radius, .95));
Caching time must be defined, the same location is used during the caching time

Default Radius:
    low: 200m
    medium: 500m
    high: 2000m

Default epsilon: 2

```js
var epsilon = st.epsilon / st.levels[level].radius;

const PlanarLaplace = require('../common/laplace');
var pl = new PlanarLaplace();
var noisy = pl.addNoise(epsilon, position.coords);

position.coords.latitude = noisy.latitude;
position.coords.longitude = noisy.longitude;

// update accuracy
if(position.coords.accuracy && st.updateAccuracy)
	position.coords.accuracy += Math.round(pl.alphaDeltaAccuracy(epsilon, .9));

// don't know how to add noise to those, so we set to null (they're most likely null anyway)	position.altitude = null;
position.altitudeAccuracy = null;
position.heading = null;
position.speed = null;

```
