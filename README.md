# Anonymization Core
**Description:** Anonymization provider for client side location privacy

**Project:** Inconnu

**Position on FIWARE Architecture:** Nonexistent on FIWARE architecture

**Code Documentation Format:** javadoc
****

## Contents
* Compilation
* Simple execution examples

### Compilation
This component of the project is meant to be integrated on the client device to provide location privacy with user-defined controls. To ease the integration with other projects it is possible to use the all the components as maven dependencies for other maven projects.

To compile and make this component available as a dependency the following must be done:
1. Install maven and openjdk-8-jdk ```sudo apt-get install maven openjdk-8-jdk```
2. Run ```maven clean install``` on this component root
3. Add the component as a dependency to other maven projects by appending the following into ```pom.xml``` dependencies tag
    ```bash
    <dependency>
      <groupId>inconnu.anonymization</groupId>
      <artifactId>core</artifactId>
      <version>0.1</version>
    </dependency>
    ```
With these steps the new project is capable to perform location privacy over the location data collected   
### Simple execution examples
Planar Laplace Geo-Indistinguishability 
```java
GeoIndistinguishabilityLaplaceSimple geoIndistinguishabilityLaplaceSimple = new GeoIndistinguishabilityLaplaceSimple(radiues,distinguishabilityLevel);
GeoLocationPoint geoLocationPoint = geoIndistinguishabilityLaplaceSimple.sanitizePoint(new GeoLocationPoint(latitude, longitude));
```

Clustered Geo-Indistinguishability
```java
GeoIndistinguishabilityLaplaceCluster geoIndistinguishabilityLaplaceCluster = new GeoIndistinguishabilityLaplaceCluster(clusterRadius,distinguishabilityLevel);
//Sanitize first point
GeoLocationPoint geoLocationPoint1 = geoIndistinguishabilityLaplaceCluster.sanitizeNewPoint(new GeoLocationPoint(latitude1, longitude1));
//Sanitize second point that uses the context of previous sanitizations to calculate the new sanitized point
GeoLocationPoint geoLocationPoint2 = geoIndistinguishabilityLaplaceCluster.sanitizeNewPoint(new GeoLocationPoint(latitude2, longitude2));
//Reset the sanitizer
geoIndistinguishabilityLaplaceCluster.reset();
```

Adaptative Geo-Indistinguishability
```java
GeoIndistinguishabilityLaplaceAdaptativeEpsilon geoIndistinguishabilityLaplaceAdaptativeEpsilon = new GeoIndistinguishabilityLaplaceAdaptativeEpsilon(radius,distinguishabilityLevel,alpha,delta1,beta,delta2)
//Sanitize first point
GeoLocationPoint geoLocationPoint1 = geoIndistinguishabilityLaplaceAdaptativeEpsilon.sanitizeNewPoint(new GeoLocationPoint(latitude1, longitude1));
//Sanitize second point that uses the context of previous sanitizations to calculate the new sanitized point
GeoLocationPoint geoLocationPoint2 = geoIndistinguishabilityLaplaceAdaptativeEpsilon.sanitizeNewPoint(new GeoLocationPoint(latitude2, longitude2)); 
```

***
For more information about the inner working of the anonymization core check the **javadoc** and the anonymization techniques articles:

**Laplace Geo-Indistinguishability Article:** "Geo-indistinguishability: Differential privacy for location-based systems"

**Laplace Geo-Indistinguishability Implementation:** https://github.com/chatziko/location-guard

**Clustered Geo-Indistinguishability Article:** Clustering geo-indistinguishability for privacy of continuous location traces

**Adaptative Geo-Indistinguishability Article:** An adaptive geo-indistinguishability mechanism for continuous LBS queries