package inconnu.anonymization.core.databases.dynamic;

import inconnu.anonymization.core.databases.dynamic.utils.GeoLocationPoint;
import inconnu.anonymization.core.databases.dynamic.utils.PlanarLaplace;

/**
 * This is the implementation of the Clustered Geo-Indistinguishability algorithm
 * This implementation follows the pseudo-code available on the paper (https://www.dcc.fc.up.pt/~joaovilela/publications/ICCCS19-Clustering_GeoInd-CR.pdf)
 */
public class GeoIndistinguishabilityLaplaceCluster {
    double epsilon;
    double radius;
    GeoLocationPoint previousC=null;
    GeoLocationPoint previousZ=null;

    /**
     * @param clusterRadius The radius on which the same sanitized location is reported
     */
    public GeoIndistinguishabilityLaplaceCluster(double clusterRadius) {
        this.epsilon = Constants.l/clusterRadius;
        this.radius = clusterRadius;
    }

    /**
     * @param clusterRadius The radius on which the same sanitized location is reported
     * @param l The distinguishability level to be applied
     */
    public GeoIndistinguishabilityLaplaceCluster(double clusterRadius,double l) {
        this.epsilon = l/clusterRadius;
        this.radius = clusterRadius;
    }


    /**This method produces a new sanitized result from the real location point  and the notion of distance from the cluster radius to produce a sanitized point
     * While the real location point is within a radius for a previous real location the same sanitized result is outputted. If the real location moves further than the radius, that location becomes  the new center cluster and all the location updates with real location with the cluster radius will have the same sanitized result
     * @param point The point to sanitize
     * @return The sanitization result
     */
    public GeoLocationPoint sanitizeNewPoint(GeoLocationPoint point){
        PlanarLaplace planarLaplace = new PlanarLaplace();
        if (previousC==null || point.distanceTo(previousC)>radius){
            GeoLocationPoint noisePoint = planarLaplace.addNoise(epsilon, point);
            this.previousZ = noisePoint;
            this.previousC = point;
            return noisePoint;
        }

        return previousZ;
    }

    /**
     * This method resets the clustered geo-indistinguishability
     */
    public void reset(){
        previousC=null;
        previousZ=null;
    }


}
