package inconnu.anonymization.core.databases.dynamic;

import inconnu.anonymization.core.databases.dynamic.utils.GeoLocationPoint;
import inconnu.anonymization.core.databases.dynamic.utils.PlanarLaplace;

/**
 * This is the implementation of the basic laplace geo-indistinshability that was published on the Geo-Indistinguishability: Differential Privacy for Location-Based Systems paper.
 * The implementation is based on the Location-Guard plugin (https://github.com/chatziko/location-guard)
 */
public class GeoIndistinguishabilityLaplaceSimple {
    double radius;
    double l;
    boolean lSet;

    /**
     * @param radius The anonymization radius
     */
    public GeoIndistinguishabilityLaplaceSimple(double radius) {
        this.radius = radius;
    }

    /**
     * @param radius The anonymization radius
     * @param l The distinguishability level
     */
    public GeoIndistinguishabilityLaplaceSimple(double radius, double l) {
        this.radius = radius;
        this.l = l;
        this.lSet=true;
    }

    /**This invokes the planar laplace and returns a sanitized point
     * @param point The point to sanitize
     * @return The sanitization result
     */
    public GeoLocationPoint sanitizePoint(GeoLocationPoint point) {
        double epsilon;
        if (lSet){
            epsilon=this.l/this.radius;
        }else epsilon=Constants.l/this.radius;

        PlanarLaplace planarLaplace = new PlanarLaplace();
        return planarLaplace.addNoise(epsilon, point);
    }

}

