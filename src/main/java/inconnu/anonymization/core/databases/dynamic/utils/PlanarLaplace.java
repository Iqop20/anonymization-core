package inconnu.anonymization.core.databases.dynamic.utils;

/**
 * Implementation transcribed from Location-Guard
 */
public class PlanarLaplace {
    final int earth_radius=6378137;

    //Degree conversions
    double rad_of_deg(double ang){
        return ang * Math.PI / 180;
    }
    double deg_of_rad(double ang){
        return ang * 180 / Math.PI;
    }

    //Mercator projection
    GeoLocationPoint getLatLon(CartPoint cartPoint) {
        double rLon = deg_of_rad(cartPoint.x / earth_radius);
        double rLat = deg_of_rad(2 * (Math.atan(Math.exp(cartPoint.y / earth_radius))) - Math.PI / 2);

        return new GeoLocationPoint(rLon,rLat);
    }

    CartPoint getCartesian(GeoLocationPoint geoLocationPoint){
        return new CartPoint(
                earth_radius*rad_of_deg(geoLocationPoint.longitude),
                earth_radius*Math.log( Math.tan(Math.PI / 4 + this.rad_of_deg(geoLocationPoint.latitude) / 2))
        );
    }

    //LambertW noise function on branch -1 (http://en.wikipedia.org/wiki/Lambert_W_function)
    double LambertW(double x){
        double min_diff = 1e-10;
        if (x== -1/Math.E){
            return -1;
        }
        if (x<0 && x>-1/Math.E){
            double q = Math.log(-x);
            double p =1;
            while (Math.abs(p-q)> min_diff){
                p=(q*q+x/Math.exp(q))/(q+1);
                q=(p*p+x/Math.exp(p))/(p+1);
            }
            return (Math.round(1000000*q)/1000000.0);
        }
        return 0;
    }

    double inverseCumulativeGamma(double epsilon,double z){
        double x = (z-1) / Math.E;
        return - (this.LambertW(x) + 1) / epsilon;
    }

    public double alphaDeltaAccuracy(double epsilon, double delta){
        return this.inverseCumulativeGamma(epsilon, delta);
    }

    double expectedError(double epsilon){
        return 2 / epsilon;
    }

    //Noise translation (http://www.movable-type.co.uk/scripts/latlong.html)
    GeoLocationPoint addVectorToPos(GeoLocationPoint pos,double distance,double angle){

        double ang_distance = distance /earth_radius;
        double lat1 = rad_of_deg(pos.latitude);
        double lon1 = rad_of_deg(pos.longitude);
        double lat2 = Math.asin(Math.sin(lat1) * Math.cos(ang_distance) + Math.cos(lat1) * Math.sin(ang_distance) * Math.cos(angle));
        double lon2 = lon1 + Math.atan2(
                Math.sin(angle) * Math.sin(ang_distance) * Math.cos(lat1),
                Math.cos(ang_distance) - Math.sin(lat1) * Math.sin(lat2)
            );
        lon2 = (lon2 + 3 * Math.PI) % (2 * Math.PI) - Math.PI;		// normalise to -180..+180

        return new GeoLocationPoint(deg_of_rad(lat2),deg_of_rad(lon2));

    }

    //Generate random noise and translate the position to the noisy one
    GeoLocationPoint addPolarNoise(GeoLocationPoint pos,double epsilon){
        double theta = Math.random() * Math.PI * 2;
        //random variable in [0,1)
        double r=0;
        double z = Math.random();
        r = this.inverseCumulativeGamma(epsilon, z);
        System.out.println("Random radius:"+r);
        System.out.println("Random angle:"+deg_of_rad(theta));
        return this.addVectorToPos(pos, r, theta);
    }


    CartPoint addPolarNoiseCartesian(double epsilon,Object position){
        if (position.getClass().toString().equals("GeoLocationPoint")){
            position = getCartesian((GeoLocationPoint) position);
        }
        CartPoint pos = (CartPoint) position;

        double theta = Math.random() * Math.PI * 2;
        //random variable in [0,1)
        double z = Math.random();
        double r = this.inverseCumulativeGamma(epsilon, z);

        return new CartPoint(
                pos.x + r * Math.cos(theta),
                pos.y + r * Math.sin(theta)
        );
    }

    public GeoLocationPoint addNoise(double epsilon,GeoLocationPoint pos){
        return addPolarNoise(pos,epsilon);
    }
}
