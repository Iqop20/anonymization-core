package inconnu.anonymization.core.databases.dynamic.utils;

/**
 * Class to hold cartesian coordinates
 */
public class CartPoint {
    double x;
    double y;

    public CartPoint(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
